<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script src="https://cdn.tailwindcss.com"></script>
	<title></title>
</head>

<body>

	<div style="width:900px;" class="container max-w-full mx-auto pt-4">

		<a href="/posts" class="bg-blue-500 tracking-wide text-white px-6 py-2 inline-block mb-6 shadow-lg rounded hover:shadow my-4">Go Back</a>


		<form method="POST" action="/posts/{{$post->id}}">
			
			@method('PUT')
			@csrf

			<div class="mb-4">
				<label class="font-bold text-gray-800" for="title">Title: </label>
				<input class="h-10 bg-white border border-gray-300 rounded py-4 px-3 mr-4 w-full text-gray-600 text-sm focus:outline-none focus:border-gray-400 focus:ring-0" id="title" name="title" value="{{$post->title}}">
			</div>


			<div class="mb-4">
				<label class="font-bold text-gray-800" for="content">Content: </label>
				<textarea class="h-16 bg-white border border-gray-300 rounded py-4 px-3 mr-4 w-full text-gray-600 text-sm focus:outline-none focus:border-gray-400 focus:ring-0" id="content" name="content">{{$post->content}}</textarea>
			</div>


			<button type="submit" class="bg-blue-500 tracking-wide text-white px-6 py-2 inline-block mb-6 shadow-lg rounded hover:shadow">Update</button>

			<a href="/posts" class="bg-gray-500 tracking-wide text-white px-6 py-2 inline-block mb-6 shadow-lg rounded hover:shadow my-4">Cancel</a>

		</form>

		<form method="POST" action="/posts/{{$post->id}}">

				@method('DELETE')
				@csrf

				<button type="submit" class="bg-red-500 tracking-wide text-white px-6 py-2 inline-block mb-6 shadow-lg rounded hover:shadow">Delete</button>

		</form>

	</div>

</body>

</html>