<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script src="https://cdn.tailwindcss.com"></script>
	<title></title>
</head>

<body>

	<div style="width:900px;" class="container max-w-full mx-auto pt-4">

		<h1 class="text-4xl font-bold mb-4">My Blog List Updated v3</h1>

		<a href="/posts/create" class="bg-blue-500 tracking-wide text-white px-6 py-2 inline-block mb-6 shadow-lg rounded hover:shadow my-4">Add Post</a>

		@foreach($posts as $post)

			<article class="mb-2">
				<a href="/posts/{{ $post->id }}/edit" class="text-xl font-bold text-blue-500">{{$post->title}}</a>

				<p class="text-md text-gray-600">{{$post->content}}</p>

			</article>

			<hr class="mt-2">
		@endforeach	

	</div>

</body>

</html>